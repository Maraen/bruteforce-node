let str = "";
let foo = 0;

const calculateHash = str => {
  let hash = str
    .split("")
    .map((c, i) => str.charCodeAt(i))
    .map(c => c + 2)
    .map(c => String.fromCharCode(c))
    .join("");
  return Buffer.from(hash).toString("base64");
};
// prettier-ignore
const alfa = [ "a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"];

let i = 0;
let j = 0;
let k = 0;
let l = 0;

while (calculateHash(str) != "ZWpxZQ==") {
  str = alfa[i] + alfa[j] + alfa[k] + alfa[l];
  l += 1;
  if (l === 26) {
    k += 1;
    l = 0;
  }
  if (k === 26) {
    j += 1;
    k = 0;
  }
  if (j === 26) {
    i += 1;
    j = 0;
  }
  if (i === 26) {
    console.log(str);
    break;
  }
  // foo += 1; //used for counting itterations.
  // console.log(foo);
}

console.log(str);
